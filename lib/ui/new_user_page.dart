import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:services/data/models/new_user.dart';
import 'package:services/di/service_locator.dart';
import 'package:services/ui/controller.dart';
import 'package:services/ui/widgets/add_user_form.dart';

class NewUserPage extends StatefulWidget {
  const NewUserPage({
    Key? key,
  }) : super(key: key);

  @override
  State<NewUserPage> createState() => _NewUserPageState();
}

class _NewUserPageState extends State<NewUserPage> {
  final homeController = getIt.get<HomeController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: const Text('Usuarios agregados recientemente'),
      ),
      body: ListView.builder(
        itemCount: homeController.newUsers.length,
        itemBuilder: (context, index) {
          final NewUser user = homeController.newUsers[index];
          return ListTile(
              onLongPress: () => {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        title: const Text('Eliminar'),
                        content: const Text(
                            '¿Seguro que desea eliminar el registro?'),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () => Navigator.pop(context, 'Cancel'),
                            child: const Text('Cancelar'),
                          ),
                          TextButton(
                            onPressed: () async {
                              await homeController
                                  .deleteNewUser(index)
                                  .then((value) {
                                setState(() {});
                              }).then((value) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                    content:
                                        Text('Usuario eliminado con éxito'),
                                    duration: Duration(seconds: 2),
                                  ),
                                );
                              });
                              // ignore: use_build_context_synchronously
                              Navigator.pop(context, 'Eliminar');
                            },
                            child: const Text('Eliminar'),
                          ),
                        ],
                      ),
                    )
                  },
              onTap: () {
                homeController.nameController.text = user.name!;
                homeController.jobController.text = user.job!;
                showModalBottomSheet(
                  context: context,
                  builder: (context) {
                    return UserForm(
                      homeController: homeController,
                      isUpdate: true,
                      onSubmit: () async {
                        await homeController
                            .updateUser(
                          index,
                          homeController.nameController.text,
                          homeController.jobController.text,
                        )
                            .then((value) {
                          Navigator.pop(context);
                          setState(() {});
                        });
                        // ignore: use_build_context_synchronously
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text('Usuario actualizado con éxito'),
                            duration: Duration(seconds: 2),
                          ),
                        );
                      },
                    );
                  },
                );
              },
              title: Text(user.name!),
              subtitle: Text(user.job!),
              trailing: user.updatedAt != null
                  ? Text(DateFormat().format(DateTime.parse(user.updatedAt!)))
                  : Text(DateFormat().format(DateTime.parse(user.createdAt!))));
        },
      ),
    );
  }
}
