import 'package:flutter/material.dart';
import 'package:services/ui/new_user_page.dart';

class BaseAppBar extends StatelessWidget with PreferredSizeWidget {
  const BaseAppBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.blueGrey,
      title: const Text('SERVICIOS WEB - DIO',
          style: TextStyle(fontWeight: FontWeight.bold)),
      actions: [
        TextButton(
          child: const Text(
            'Nuevos usuarios ',
            style: TextStyle(
              color: Colors.white,
              fontSize: 18
            ),
          ),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const NewUserPage(),
              ),
            );
          },
        ),
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
