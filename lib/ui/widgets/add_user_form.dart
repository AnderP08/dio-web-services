import 'package:flutter/material.dart';
import 'package:services/ui/controller.dart';

class UserForm extends StatelessWidget {
  const UserForm(
      {Key? key,
      required this.homeController,
      this.isUpdate = false,
      required this.onSubmit})
      : super(key: key);

  final HomeController homeController;
  final bool? isUpdate;
  final VoidCallback onSubmit;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextField(
            controller: homeController.nameController,
            decoration: const InputDecoration(
              labelText: 'Nombres',
              hintText: 'Agregar nombres'
            ),
          ),
          TextField(
            controller: homeController.jobController,
            decoration: const InputDecoration(
              labelText: 'Ocupación',
            ),
          ),
          const SizedBox(height: 30),
          ElevatedButton(
            style: const ButtonStyle(backgroundColor: MaterialStatePropertyAll(Colors.blueGrey)),
            onPressed: onSubmit,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(isUpdate! ? 'Actualizar' : 'Agregar'),
            ),
          ),
        ],
      ),
    );
  }
}
