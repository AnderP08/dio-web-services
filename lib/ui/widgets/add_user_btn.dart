import 'package:services/di/service_locator.dart';
import 'package:services/ui/controller.dart';
import 'package:flutter/material.dart';
import 'package:services/ui/new_user_page.dart';
import 'package:services/ui/widgets/add_user_form.dart';

class AddUserBtn extends StatelessWidget {
  AddUserBtn({
    Key? key,
  }) : super(key: key);

  final homeController = getIt<HomeController>();

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      backgroundColor: Colors.blueGrey,
      onPressed: () {
        showModalBottomSheet(
          context: context,
          builder: (context) {
            return UserForm(
              homeController: homeController,
              onSubmit: () async {
                await homeController.addNewUser();
                // ignore: use_build_context_synchronously
                Navigator.pop(context);
                homeController.nameController.clear();
                homeController.jobController.clear();
                // ignore: use_build_context_synchronously
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => const NewUserPage(),
                  ),
                );
              },
            );
          },
        );
      },
      child: const Icon(Icons.add),
    );
  }
}
